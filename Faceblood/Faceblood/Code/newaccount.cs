﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Faceblood;
using System.Net;
using Android.Content;
using PageDeLogin;
using System.Linq;

namespace App1
{
    [Activity(Label = "Création du compte")]
    public class NewAccount : Activity
    {
        string toto = "";
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            WebClient client = new WebClient();


            // Set our view from the "Main" layout resource
            SetContentView(Resource.Layout.layeraccount);

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner);
            EditText pseudo = FindViewById<EditText>(Resource.Id.pseudo);
            EditText password = FindViewById<EditText>(Resource.Id.password);
            EditText passswordRepeat = FindViewById<EditText>(Resource.Id.passwordRepeat);
            EditText CP = FindViewById<EditText>(Resource.Id.Codepostal);
            Button go = FindViewById<Button>(Resource.Id.button2);

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);

            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.Groupe_sanguin, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;


            go.Click += delegate {
                if (password.Text == passswordRepeat.Text)
                {
                    string resus = "";
                    if (toto.Contains("+")){
                         resus = "pos";
                    }
                    else
                    {
                         resus = "neg";
                    }
                    var mystring = String.Join("", toto.Split('+', '-')); ;
                    string url = "http://192.168.56.101/creationcompte.php?type=donneur&username="+pseudo.Text+"&pass="+password.Text+"&groupe="+mystring+"&cp="+CP.Text+"&resus="+resus;
                    string vals = client.DownloadString(url);

                    var myPage = new Intent(this, typeof(Login));
                    StartActivity(myPage);                    
                }
            };
        }
        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            toto = string.Format("{0}", spinner.GetItemAtPosition(e.Position));
            toto = toto.ToString();
        }     




    }
}

