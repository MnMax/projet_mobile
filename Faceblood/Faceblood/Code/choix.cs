﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using PageDeLogin;
using LogUser;
using PageHop;
using App1;
using Faceblood;
using CreaHopital;

namespace ChoixCreation
{
    [Activity(Label = "Test")]
    public class choixCompte : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.choix_type);


            Button nouveauUser = FindViewById<Button>(Resource.Id.creaUser);
            Button nouveauHopital = FindViewById<Button>(Resource.Id.CreaHopital);

            nouveauUser.Click += delegate {
                var myPage = new Intent(this, typeof(NewAccount));
                StartActivity(myPage);
            };

            nouveauHopital.Click += delegate {
                var myPage = new Intent(this, typeof(NewAccountHopital));
                StartActivity(myPage);
            };
        }
    }
}

