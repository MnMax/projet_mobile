﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content;
using PageDeLogin;
using LogUser;
using PageHop;
using App1;
using ChoixCreation;

namespace Faceblood
{
    [Activity(Label = "Faceblood", MainLauncher = true)]
    public class MainActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Main);

            var prefs = Application.Context.GetSharedPreferences("Faceblood", FileCreationMode.Private);
            var myToken = prefs.GetString("TokenSaved", null);
            var myAccount = prefs.GetString("accountType", null);


            /*if (!string.IsNullOrEmpty(myToken))
            {
                if(myAccount == "donneur")
                {
                    var myPage = new Intent(this, typeof(LogUserpage));
                    myPage.PutExtra("token", myToken);
                    StartActivity(myPage);
                }
                else if (myAccount == "hopital")
                {
                    var myPage = new Intent(this, typeof(LoginHop));
                    myPage.PutExtra("token", myToken);
                    StartActivity(myPage);
                }
            }*/
            

            Button connect = FindViewById<Button>(Resource.Id.btnSignIn);
            Button create = FindViewById<Button>(Resource.Id.btnSignUp);

            connect.Click += delegate {
                var myPage = new Intent(this, typeof(Login));
                StartActivity(myPage);
            };

            create.Click += delegate {
                var myPage = new Intent(this, typeof(choixCompte));
                StartActivity(myPage);
            };
        }
    }
}

