﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;
using Faceblood;
using System.Data.SqlClient;
//using Android;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Linq;
using System.Text;

namespace LogUser
{
    [Activity(Label = "Mon Compte")]
    public class LogUserpage : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            WebClient client = new WebClient();
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.coUser);
            TextView myText = FindViewById<TextView>(Resource.Id.messageUser);

            //Recuperation du token de connexion
            string token = Intent.GetStringExtra("token") ?? "Erreur";
       
            //Recuperation des valeurs de pseudo et groupe de l'utilisateur
            string url = "http://192.168.56.101/user.php?token="+token;
            string vals = client.DownloadString(url);

            string[] vars = vals.Split(';');
            string nom = vars[0];
            string groupe = vars[1];
            string demande = vars[2];

            if(demande == "oui")
            {
                myText.Text = "Bonjour " + nom + " votre groupe est " + groupe+" de plus ce dernier est demandé dans l'hopital le plus proche";

            }
            else
            {
                myText.Text = "Bonjour " + nom + " votre groupe est " + groupe;
            }

            //Affichage d'un message gentil <3


        }
       protected override void OnDestroy()
        {
            base.OnDestroy();
            string token = Intent.GetStringExtra("token") ?? "Erreur";

            var prefs = Application.Context.GetSharedPreferences("Faceblood", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("TokenSaved", token);
            prefEditor.PutString("accountType", "donneur");
            prefEditor.Commit();
        }

       protected override void OnStop()
        {
            base.OnStop();
            string token = Intent.GetStringExtra("token") ?? "Erreur";

            var prefs = Application.Context.GetSharedPreferences("Faceblood", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("TokenSaved", token);
            prefEditor.PutString("accountType", "donneur");
            prefEditor.Commit();
        }
    }
}
