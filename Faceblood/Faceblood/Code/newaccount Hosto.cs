﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Faceblood;
using System.Net;
using Android.Content;
using PageDeLogin;
using System.Linq;

namespace CreaHopital
{
    [Activity(Label = "Création du compte Hopital")]
    public class NewAccountHopital : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            WebClient client = new WebClient();
            // Set our view from the "Main" layout resource
            SetContentView(Resource.Layout.creationHopital);
            EditText pseudo = FindViewById<EditText>(Resource.Id.pseudoHopital);
            EditText password = FindViewById<EditText>(Resource.Id.passwordHopital);
            EditText passswordRepeat = FindViewById<EditText>(Resource.Id.passwordHopitalConfirm);
            EditText CP = FindViewById<EditText>(Resource.Id.CPHopital);
            Button go = FindViewById<Button>(Resource.Id.button10);


            go.Click += delegate {
                if (password.Text == passswordRepeat.Text)
                {
                    string url = "http://192.168.56.101/creationcompte.php?type=hopital&username=" + pseudo.Text + "&pass=" + password.Text +"&cp=" + CP.Text;
                    string vals = client.DownloadString(url);

                    var myPage = new Intent(this, typeof(Login));
                    StartActivity(myPage);
                }
            };


        }
    }
}

