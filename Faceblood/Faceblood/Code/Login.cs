﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;
using Faceblood;
using Android;
using System.Net;
using LogUser;
using PageHop;

namespace PageDeLogin
{
    [Activity(Label = "Login")]
    public class Login : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            WebClient client = new WebClient();
            base.OnCreate(savedInstanceState);
            SetContentView(Faceblood.Resource.Layout.connect);


            Button buttonCo = FindViewById<Button>(Faceblood.Resource.Id.buttonConnect);
            EditText login = FindViewById<EditText>(Faceblood.Resource.Id.pseudo);
            EditText password = FindViewById<EditText>(Faceblood.Resource.Id.password);


                // When the user clicks the button ...
                buttonCo.Click += (sender, e) =>
                {

                    string url = "http://192.168.56.101?name=" + login.Text + "&pass=" + password.Text;
                    //string token = client.DownloadString(url);

                    string vals = client.DownloadString(url);

                    string[] vars = vals.Split(';');
                    string token = vars[0];
                    


                    if (!string.IsNullOrEmpty(token))
                    {
                        string compte = vars[1];
                        if (compte == "donneur")
                        {
                            var myPage = new Intent(this, typeof(LogUserpage));
                            myPage.PutExtra("token", token);
                            StartActivity(myPage);
                        }
                        else if (compte == "hopital")
                        {
                            var myPage = new Intent(this, typeof(LoginHop));
                            myPage.PutExtra("token", token);
                            StartActivity(myPage);
                        }
                        else
                        {
                            var myPage = new Intent(this, typeof(Login));
                            StartActivity(myPage);
                        }

                    }
                    else
                        Toast.MakeText(this, "Connexion Impossible", ToastLength.Short).Show();

                };




            
        }
    }
}

