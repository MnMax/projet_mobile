﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;
using Faceblood;
using System.Net;
using LogUser;

namespace Demande
{
    [Activity(Label = "Page de gestion Hopital")]
    public class ClassDemande : Activity
    {
        string toto = "";

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.lancement_alerte);
            WebClient client = new WebClient();


            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner);

            Button go = FindViewById<Button>(Resource.Id.buttonDemande);

            string CP = Intent.GetStringExtra("CP") ?? "Erreur";

            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);

            var adapter = ArrayAdapter.CreateFromResource(
                this, Resource.Array.Groupe_sanguin, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;


            go.Click += delegate {

                string resus = "";
                if (toto.Contains("+"))
                {
                    resus = "pos";
                }
                else
                {
                    resus = "neg";
                }
                var mystring = String.Join("", toto.Split('+', '-')); ;
                string url = "http://192.168.56.101/demande.php?groupe=" + mystring + "&cp=" + CP + "&resus=" + resus;
                string vals = client.DownloadString(url);
                Toast.MakeText(this, "Vous demandez : "+mystring+resus, ToastLength.Short).Show();




            };
    }
    private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
    {
        Spinner spinner = (Spinner)sender;
        toto = string.Format("{0}", spinner.GetItemAtPosition(e.Position));
        toto = toto.ToString();
    }


    }
    }