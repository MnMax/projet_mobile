﻿using System;
using Android.Content;
using Android.App;
using Android.Widget;
using Android.OS;
using Faceblood;
using System.Net;
using LogUser;
using Demande;

namespace PageHop
{
    [Activity(Label = "Page de gestion Hopital")]
    public class LoginHop : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            WebClient client = new WebClient();
            base.OnCreate(savedInstanceState);
            SetContentView(Faceblood.Resource.Layout.coHop);

            TextView myText = FindViewById<TextView>(Resource.Id.messageUser);
            Button monBouton = FindViewById<Button>(Resource.Id.buttonDemande);
            //Recuperation du token de connexion
            string token = Intent.GetStringExtra("token") ?? "Erreur";

            //Recuperation des valeurs de pseudo et groupe de l'utilisateur
            string url = "http://192.168.56.101/hopital.php?token=" + token;
            string vals = client.DownloadString(url);

            string[] vars = vals.Split(';');
            string nom = vars[0];
            string CP = vars[1];

            //Affichage d'un message gentil <3
            myText.Text = "Bonjour " + nom + " vous pouvez faire une demande de sang pour les personnes se trouvant dans le " + CP;

            monBouton.Click += delegate
            {
                var myPage = new Intent(this, typeof(ClassDemande));
                myPage.PutExtra("CP", CP);
                StartActivity(myPage);
            };
  
        }


        protected override void OnDestroy()
        {
            base.OnDestroy();

            string token = Intent.GetStringExtra("token") ?? "Erreur";

            var prefs = Application.Context.GetSharedPreferences("Faceblood", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("TokenSaved", token);
            prefEditor.PutString("accountType", "hopital");
            prefEditor.Commit();
        }
        protected override void OnStop()
        {
            base.OnStop();
            string token = Intent.GetStringExtra("token") ?? "Erreur";

            var prefs = Application.Context.GetSharedPreferences("Faceblood", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("TokenSaved", token);
            prefEditor.PutString("accountType", "hopital");
            prefEditor.Commit();
        }

    }
 }